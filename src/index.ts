import iohook from "iohook";
import fs from "fs"
import chalk from "chalk"
import keycodes from "keycode"
import robotjs from "robotjs"
import clipboardy from "clipboardy"

const config = JSON.parse(fs.readFileSync("data/config.json").toString())
const keyDefs = JSON.parse(fs.readFileSync("data/texts.json").toString())

let isInSelection = false;
const activator = Number(config.activator);
let relevantKeyCodes : number[] = [activator];

let locked: boolean = false;

let displayTree : any = {};
iohook.on("keypress", async function(msg){
    if(locked){
        return;
    }
    locked = true;
    const pressedKey : number = msg.rawcode;
    // If actively listening for that key...
    if(relevantKeyCodes.some(e => e === pressedKey)){
        if(pressedKey === activator){
            handleSelectionSwap();
        }else{
            if(typeof displayTree[pressedKey] === "object"){
                displayTree = displayTree[pressedKey]
                relevantKeyCodes = [activator,...getKeysToListen(displayTree)];
            }else if(typeof displayTree[pressedKey] === "string"){
                await printString(displayTree[pressedKey])
                await new Promise<void>(e => setTimeout(f => e(), 500))
                handleSelectionSwap();
            }
        }
        // Redraw tree at the end
        printState();
    }
    // If not in set of keys to listen to... ignore
    locked = false;
})

iohook.start();
printState();

function handleSelectionSwap() : void{
    if(isInSelection){
        isInSelection = false;
        displayTree = {}
        relevantKeyCodes = [activator]
    }else{
        isInSelection = true;
        displayTree = keyDefs;
        relevantKeyCodes = [activator, ...getKeysToListen(displayTree)]
    }
}

function getKeysToListen(jsonNode: Object) : number[]{
    return Object.keys(jsonNode)
        .filter(e => Number.isInteger(Number(e)))
        .map(e => Number(e))
        .filter(e => e >= 0)
}

function printState(){
    const trimmedMessage = function(str : string){
        if(str.length < 50){
            return str;
        }
        return str.substr(0,47) + "..."
        
    }


    console.clear();
    relevantKeyCodes.forEach( e => {
        if(e === activator){
            console.log(chalk.green(`[${keycodes(e)}]`), " > ", chalk.yellow(!isInSelection ? "Select": "Cancel"))
        }else{
            if(typeof displayTree[e] === "string"){
                console.log(chalk.greenBright(`[${keycodes(e)}]`), " > ",chalk.grey(trimmedMessage(displayTree[e])));
            }
            else{
                console.log(chalk.green(`[${keycodes(e)}]`), " > ", chalk.white("<Expand Node>"));
            }
        }
    })
    
}

async function printString(str: string){
    console.clear();
    console.info(chalk.yellow("Printing..."));
    for(let i = 0; i < 5; i++){
        robotjs.keyTap("backspace")
    }
    if(str.length > 255){
        console.info(chalk.red("Cannot print multi-message messages at this time."))
        return await wait(500)
    }

    // Split String into 100-Character segments. 
    // This is done because Elite does not allow pastes to be longer than 100 chars.
    const subPastes = str.match(/.{1,100}/g)
    
    for(const sub of subPastes as string[]){
        // Foreach segment, paste it to the clipboard buffer
        clipboardy.writeSync(sub);
        console.info(chalk.gray(sub))
        // And then hit Ctrl+V to paste
        await wait(50)
        robotjs.keyTap("v","control")
        await wait(50)
        robotjs.keyTap("control")
        await wait(50)
    }
    // The Enter was removed. The user should enter on their own because 
    // Elite tends to be abit... inconsistent.
    // After a message has been rapidly entered, the UI trips up.
    // Sometimes one Enter suffices, sometimes you need more.

    //await wait(100)
    //robotjs.keyTap("enter")
    
    // Tap CTRL in case E:D tripped up on it.
    robotjs.keyTap("control")
}

async function wait(ms: number){
    await new Promise(e => setTimeout((e), ms))
}